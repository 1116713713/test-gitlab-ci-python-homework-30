import pytest
from fastapi.testclient import TestClient
from src.main import app, get_db
from src.models import Base

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import StaticPool


DATABASE_URL = "sqlite:///.app.py.db"

engine = create_engine(DATABASE_URL, connect_args={"check_same_thread": False},
    poolclass=StaticPool)

TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base.metadata.create_all(bind=engine)

def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db


client = TestClient(app)

JSON = {
    "name": "ratatouille",
    "cooking_time": 50,
    "ingredients": [
        {
            "name": "garlic",
            "count": 1
        },
        {
            "name": "bell pepper",
            "count": 2
        },
        {
            "name": "tomato",
            "count": 5
        }
    ],
    "description": "Ratatouille is a traditional vegetable dish of Provençal cuisine made from peppers, eggplants and zucchini, much like Hungarian lecso"
}


def test_create_recite():
    response = client.post(
        "/api/recipe/",
        json=JSON,
    )
    assert response.status_code == 200
    response_json = JSON.copy()
    response_json["id"] = 1
    assert response.json() == [response_json]


def test_get_recipe_detail_id():
    response = client.get(
        "/api/recipe/detail/1"
    )
    assert response.status_code == 200
    response_json = JSON.copy()
    response_json["id"] = 1
    assert response.json() == [response_json]


def test_get_all_recipe_detail():
    response = client.get(
        "/api/recipe/detail"
    )
    assert response.status_code == 200
    response_json = JSON.copy()
    response_json["id"] = 1
    assert response.json() == [response_json]


def test_get_all_recipe():
    response = client.get(
        "/api/recipe"
    )
    assert response.status_code == 200
    response_json = {
        "name": "ratatouille",
        "cooking_time": 50,
        "views": 2,
        "id": 1
    }
    assert response.json() == [response_json]
