from typing import Dict, List

from fastapi import FastAPI, Path
from sqlalchemy.future import select

import src.models as models  # type: ignore
from src.database import engine, session  # type: ignore
from src.shemas import BaseRecipeOut, RecipeDetailIn, RecipeDetailOut  # type: ignore


def get_db():
    db = session
    try:
        yield db
    finally:
        db.close()


app = FastAPI()


@app.on_event("startup")
async def startup():
    async with engine.begin() as conn:
        await conn.run_sync(models.Base.metadata.create_alliso)


@app.on_event("shutdown")
async def shutdown():
    await session.close()
    await engine.dispose()


@app.get("/api/recipe/", response_model=List[BaseRecipeOut])
async def all_recipes() -> List[models.Recipes]:
    """Endpoint возвращающий краткое описание и
    количество просмотров всех рецептов"""
    result = await session.execute(
        select(models.Recipes).order_by(
            models.Recipes.views.desc(), models.Recipes.cooking_time.desc()
        )
    )

    return result.scalars().all()


@app.get("/api/recipe/detail", response_model=List[RecipeDetailOut])
async def all_recipe_detail() -> List[models.Recipes]:
    """Endpoint возвращающий детальное описание со списком ингредиентов,
    так же обновляет просмотры всех рецептов"""
    recipes = await session.execute(select(models.Recipes))
    recipes = recipes.all()

    # Обновляем просмотры у всех рецептов
    for i_recipe in recipes:
        i_recipe[0].views += 1
    await session.commit()

    recipe = await session.execute(
        select(models.Recipes, models.Ingredients).join(models.Ingredients)
    )
    recipe = recipe.all()

    new_recipes: List[Dict] = list()
    # Генерируем список рецептов
    for i_recipe in recipe:
        if (not new_recipes) or (new_recipes[-1]["id"] != i_recipe.Recipes.id):
            new_recipes.append(i_recipe.Recipes.to_json())
            new_recipes[-1]["ingredients"] = list()

        new_recipes[-1]["ingredients"].append(i_recipe.Ingredients.to_json())

    return new_recipes


@app.get("/api/recipe/detail/{id}", response_model=List[RecipeDetailOut])
async def recipe_detail_id(
    id: int = Path(..., title="Recipe ID")
) -> List[models.Recipes]:
    """Endpoint возвращающий детальное описание со списком ингредиентов,
    так же обновляет просмотры рецепта по ID"""
    recipe = await session.execute(
        select(models.Recipes, models.Ingredients)
        .join(models.Ingredients)
        .filter(models.Ingredients.recipe_id == id)  # type: ignore
    )
    recipe = recipe.all()

    # Обновляем просмотры
    recipe[0].Recipes.views += 1
    await session.commit()

    result_list = list()
    # Генерируем рецепт
    result_list.append(recipe[0].Recipes.to_json())
    result_list[0]["ingredients"] = list()
    for i_recipe in recipe:
        result_list[0]["ingredients"].append(i_recipe.Ingredients.to_json())

    return result_list


@app.post("/api/recipe/", response_model=List[RecipeDetailOut])
async def recipe_detail(recipe: RecipeDetailIn) -> List[models.Recipes]:
    """Endpoint создающий новый объект и добавляющий его в БД"""
    new_recipe = models.Recipes(
        name=recipe.name,
        cooking_time=recipe.cooking_time,
        description=recipe.description,
    )
    for i_ingredient in recipe.ingredients:
        new_ingredient = models.Ingredients(
            name=i_ingredient.name, count=i_ingredient.count
        )
        new_recipe.ingredient.append(new_ingredient)

    async with session:
        session.add(new_recipe)
        await session.commit()

    # Генерируем рецепт
    new_recipe_response = [new_recipe.to_json()]
    new_recipe_response[0]["ingredients"] = list()
    for i_ingredient in new_recipe.ingredient:
        new_recipe_response[0]["ingredients"].append(i_ingredient.to_json())

    return new_recipe_response
