from sqlalchemy import Column, ForeignKey, Integer, String, Text
from sqlalchemy.orm import Relationship

from src.database import Base  # type: ignore


class Recipes(Base):
    __tablename__ = "recipes"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True, nullable=False)
    cooking_time = Column(Integer, index=True, nullable=False)
    description = Column(Text, index=True, nullable=False)
    views = Column(Integer, index=True, default=0)
    ingredient = Relationship("Ingredients", back_populates="recipe")  # type: ignore

    def to_json(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Ingredients(Base):
    __tablename__ = "ingredients"

    id = Column(Integer, primary_key=True)
    name = Column(String, index=True, nullable=False)
    count = Column(Integer, index=True, nullable=False)
    recipe_id = Column(Integer, ForeignKey("recipes.id"))
    recipe = Relationship("Recipes", back_populates="ingredient")  # type: ignore

    def to_json(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
