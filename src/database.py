from sqlalchemy.ext.asyncio import AsyncEngine, AsyncSession, create_async_engine

# from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import declarative_base, sessionmaker
from sqlalchemy.orm.decl_api import DeclarativeMeta

DATABASE_URL = "sqlite+aiosqlite:///.app.py.db"

engine: AsyncEngine = create_async_engine(DATABASE_URL, echo=True)

async_session = sessionmaker(engine, expire_on_commit=False, class_=AsyncSession)  # type: ignore
session: AsyncSession = async_session()
Base: DeclarativeMeta = declarative_base()
