from typing import List

from pydantic import BaseModel


class BaseRecipe(BaseModel):
    name: str
    cooking_time: int
    views: int


class BaseRecipeIn(BaseRecipe): ...


class BaseRecipeOut(BaseRecipe):
    id: int

    class Config:
        orm_mode = True


class Ingredient(BaseModel):
    name: str
    count: int


class IngredientIn(Ingredient): ...


class IngredientOut(Ingredient):
    id: int

    class Config:
        orm_mode = True


class RecipeDetail(BaseModel):
    name: str
    cooking_time: int
    ingredients: List[Ingredient]
    description: str


class RecipeDetailIn(RecipeDetail): ...


class RecipeDetailOut(RecipeDetail):
    id: int

    class Config:
        orm_mode = True
